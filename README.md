## Module 7: RDS

### Sub-task 1 - Create RDS instance

**1. Create an RDS instance in the private <ProjectName>-DbSubnet-RDS subnet**

![1.1.1](screenshots/1.1.1.png)

![1.1.2](screenshots/1.1.2.png)

![1.1.3](screenshots/1.1.3.png)

**2. Ensure Main microservice EC2 instance has correctly configured security group to access RDS.**

![1.3](screenshots/1.3.png)

**3. Make sure, Main microservice EC2 instance has access to already created RDS instance**


![1.4](screenshots/1.4.png)



### Sub-task 2 - Migrate your on-premise DB schema to RDS

**1. Set up Replication instance**

![2.2](screenshots/2.2.png)

**2. Set Up Source and Target Endpoints**

![2.3](screenshots/2.3.png)

### Sub-task 3 - Update your Main microservice

**1. Update your Main microservice application configuration with the newly created RDS database instance property values.**

![3.1](screenshots/3.1.png)

**2. Replace .tar Docker image uploaded to S3 in Module 5: EC2 with the new one.**

![3.2](screenshots/3.2.png)

**3. Update docker image.**

![3.3](screenshots/3.3.png)

**4. Run updated Main microservice EC2 instance. Verify that application is connected to RDS instance**

![3.4](screenshots/3.4.png)

**5. Make some calls to created in step 3 microservice instance.Verify that integration with RDS works as expected.**

![3.5](screenshots/3.5.png)

![3.6](screenshots/3.6.png)